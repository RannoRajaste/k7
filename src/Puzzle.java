import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.HashSet;
import java.util.Collections;

public class Puzzle {

   /*
    * Permutatsioonide genereerimiseks on kasutatud meetodites generateAllPermutations(), enumerate() ja swap()
    * modifitseeritud kujul koodi allikast
    * https://stackoverflow.com/questions/44949030/print-all-possible-permutations-of-r-elements-in-a-given-integer-array-of-size-n
    * Kogu ülejäänud kood ja loogika on omalooming.
    * */

   private static int numberOfSolutions;
   private static String firstWord;
   private static String secondWord;
   private static String thirdWord;
   private static String[] uniqueLetters;
   private static final Integer[] possibleNumbers = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
   private static HashMap<String, Integer> letterMap;

   public static void main(String[] args) {
      String invalidArgumentsMessage = String.format("Invalid input: expected 3 words but received %s", args.length);
      if (args.length != 3){
         throw new InvalidInputException(invalidArgumentsMessage);
      }
      for (String arg: args) {
         checkIfWordIsCorrect(arg);
      }

      numberOfSolutions = 0;
      firstWord = args[0];
      secondWord = args[1];
      thirdWord = args[2];

      printInput();
      getAllUniqueLetters();
      long startTime = System.currentTimeMillis();
      generateAllPermutations(Arrays.asList(possibleNumbers), uniqueLetters.length);
      long endTime = System.currentTimeMillis();

      printNumberOfSolutions();
      printTimeTaken(endTime - startTime);
   }

   public static void generateAllPermutations(List<Integer> seed, int numberOfElementsInPermutation) {
      ArrayList<ArrayList<Integer>> allPermutations = new ArrayList<>();
      enumerate(seed, seed.size(), numberOfElementsInPermutation, allPermutations);
   }

   private static void enumerate(List<Integer> seed, int seedSize, int numberOfElementsInPermutation, List<ArrayList<Integer>> allPermutations) {
      if (numberOfElementsInPermutation == 0) {
         ArrayList<Integer> singlePermutation = new ArrayList<>();
         for (int i = seedSize; i < seed.size(); i++){
            singlePermutation.add(seed.get(i));
         }
         checkEquation(singlePermutation.toArray(new Integer[0]));
         allPermutations.add(singlePermutation);
         return;
      }

      for (int i = 0; i < seedSize; i++) {
         swap(seed, i, seedSize - 1);
         enumerate(seed, seedSize - 1, numberOfElementsInPermutation-1, allPermutations);
         swap(seed, i, seedSize - 1);
      }
   }

   public static void swap(List<Integer> a, int i, int j) {
      Integer temp = a.get(i);
      a.set(i, a.get(j));
      a.set(j, temp);
   }

   public static void getAllUniqueLetters(){

      HashSet<String> letterSet = new HashSet<>();
      Collections.addAll(letterSet, firstWord.split(""));
      Collections.addAll(letterSet, secondWord.split(""));
      Collections.addAll(letterSet, thirdWord.split(""));
      uniqueLetters = letterSet.toArray(new String[0]);

   }

   public static void checkEquation(Integer[] seed){
      generateLetterMap(seed);
      long firstWordValue;
      long secondWordValue;
      long thirdWordValue;

      try {
         firstWordValue = getWordValue(firstWord);
         secondWordValue = getWordValue(secondWord);
         thirdWordValue = getWordValue(thirdWord);
      } catch (InvalidWordValueException e) {
         return;
      }

      if (isEquationCorrect(firstWordValue, secondWordValue, thirdWordValue)){
         numberOfSolutions++;
         if (isFirstSolution()) {
            printSolution();
         }
      }
   }

   public static Long getWordValue(String word){
      StringBuilder stringBuilder = new StringBuilder();
      String[] wordParts = word.split("");
      checkIfWordValueStartsWithZero(wordParts);
      for (String letter : wordParts) {
         stringBuilder.append(letterMap.get(letter));
      }
      return Long.parseLong(stringBuilder.toString());
   }

   public static void generateLetterMap(Integer[] seed){
      HashMap<String, Integer> generatedMap = new HashMap<>();
      for (int i = 0; i < seed.length; i++) {
         generatedMap.put(uniqueLetters[i], seed[i]);
      }
      letterMap = generatedMap;
   }

   public static boolean isEquationCorrect(long a, long b, long c){
      return a + b == c;
   }

   public static boolean isFirstSolution(){
      return numberOfSolutions == 1;
   }

   public static void checkIfWordValueStartsWithZero(String[] wordParts) {
      if (letterMap.get(wordParts[0]) == 0) {
         throw new InvalidWordValueException("Word value cannot start with zero");
      }
   }

   public static void checkIfWordIsCorrect(String word){
      if (!word.toUpperCase().equals(word)){
         throw new InvalidInputException("All words must be in uppercase");
      } else if (!word.matches("^[A-Z]+$")){
         throw new InvalidInputException("Words can only contain latin letters");
      }
   }

   public static void printSolution(){
      String solution = letterMap
              .toString()
              .replace("{", "")
              .replace("}", "");

      System.out.println("Solution: " + solution);
   }

   public static void printInput(){
      System.out.println(String.format("Puzzle: %s + %s = %s", firstWord, secondWord, thirdWord));
   }

   public static void printNumberOfSolutions(){
      System.out.println("Number of all possible solutions: " + numberOfSolutions);
   }

   public static void printTimeTaken(long millis){
      System.out.println(String.format("Time: %s seconds", millis /1000.0));
   }

   static class InvalidWordValueException extends RuntimeException {
      public InvalidWordValueException(String msg){
         super(msg);
      }
   }

   static class InvalidInputException extends RuntimeException {
      public InvalidInputException(String msg){
         super(msg);
      }
   }
}
